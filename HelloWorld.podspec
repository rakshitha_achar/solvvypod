#
#  Be sure to run `pod spec lint HelloWorld.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#
Pod::Spec.new do |s|
# ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #
s.name          = "HelloWorld"
s.version       = "5.0"
s.summary       = "HelloWorld is a framework"
s.homepage      = "https://gitlab.com/rakshitha_achar"
s.description   = "HelloWorld is a swift framework which has a logger class and printHelloWorld func with ymedialabs as output"
s.license       = "MIT"
s.author        = { "rakshitha_achar" => "rakshitha@ymedialabs.com" }
s.platform      = :ios, "10.0"
s.ios.vendored_frameworks = 'HelloWorld.framework'
s.swift_version = "5.0"
s.source        = { :git => "https://gitlab.com/rakshitha_achar/SolvvyPod.git", :tag => "#{s.version}" }
s.exclude_files = "Classes/Exclude"
end